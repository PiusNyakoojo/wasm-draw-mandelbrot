import * as gulp from 'gulp'
import * as shell from 'gulp-shell'
import * as ts from 'gulp-typescript'
import gulpTslint from 'gulp-tslint'
import * as tslint from 'tslint'

import * as runSequence from 'run-sequence'

import envVariables from './env_variables'

// Provide the build environment variables globally for
// the rest of this file to use.
let {
  PATH_SRC,
  PATH_SRC_DIST,
  PATH_BUILD,
  PATH_BUILD_DIST
} = envVariables

// A task for using the Emscripten compiler to generate
// WebAssembly code from a source file.
function task_emscripten (src: string, dest: string, jsLibrary: string) {
  return () => {
    return shell.task(
      `emcc ${src} -o ${dest} --js-library ${jsLibrary} -s EXTRA_EXPORTED_RUNTIME_METHODS=["ccall", "cwrap"]`
    )
  }
}

// A task for generating a single JavaScript file from a
// collection of files using the Webpack bundler.
function task_bundle_js (webpackFile: string) {
  return () => {
    return shell.task(`webpack --config ${webpackFile}`)
  }
}

// A task for compiling TypeScript (.ts) files to JavaScript (.js) files.
function task_typescript (files: string[], destination: string) {
  return () => {
    let tsProject = ts.createProject(`${PATH_SRC}/tsconfig.json`)
    return gulp.src(files)
      .pipe(tsProject())
      .pipe(gulp.dest(destination))
  }
}

// A task for linting TypeScript files.
function task_tslint (files: string[]) {
  return () => {
    let program = tslint.Linter.createProgram(`${PATH_SRC}/tsconfig.json`)

    return gulp.src(files, { base: '.' })
      .pipe(gulpTslint({
        program,
        formatter: 'verbose'
      }))
      .pipe(gulpTslint.report())
  }
}

// A task for running multiple gulp tasks
// sequentially (e.g. one after another).
function task_chain (taskList: string[], callback?: Function) {
  return (done: Function) => {
    runSequence(...taskList, () => {
      done()
      callback()
    })
  }
}

// A task for removing the files in a directory.
function task_clean_dir (dir: string) {
  return () => {
    return shell.task(`rm -rf ${dir}/*`)
  }
}

// A task for copying a file from one place to another.
function task_copy_file (fileSrc: string, destination: string) {
  return () => {
    return gulp.src(fileSrc)
      .pipe(gulp.dest(destination))
  }
}

export {
  task_emscripten,
  task_bundle_js,
  task_typescript,
  task_tslint,
  task_chain,
  task_clean_dir,
  task_copy_file
}
