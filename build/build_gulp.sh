
# Apply a TypeScript linter to check for potential code
# formatting errors with the TypeScript build files.
node_modules/.bin/tslint './build/*.ts' --config './tslint.json'

# Transpile the TypeScript build files to JavaScript.
node_modules/.bin/tsc --project './build/tsconfig.json'
