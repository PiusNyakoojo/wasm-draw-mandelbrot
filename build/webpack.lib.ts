import * as path from 'path'

import envVariables from './env_variables'

// Provide the build environment variables globally for
// the rest of this file to use.
let {
  PATH_SRC_DIST,
  WASM_FILEPATH_JS_LIB
} = envVariables

// Export Webpack configuration settings.
export default {
  entry: WASM_FILEPATH_JS_LIB,
  output: {
    path: path.resolve(PATH_SRC_DIST, './lib'),
    filename: 'index.js'
  }
}
