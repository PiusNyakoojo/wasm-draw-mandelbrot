import * as gulp from 'gulp'

import {
  task_emscripten,
  task_bundle_js,
  task_typescript,
  task_tslint,
  task_chain,
  task_clean_dir,
  task_copy_file
} from './gulp_functions'

import envVariables from './env_variables'

// Provide the build environment variables globally for
// the rest of this file to use.
let {
  PATH_SRC,
  PATH_SRC_DIST,
  PATH_BUILD,
  PATH_BUILD_DIST,
  WEBPACK_FILEPATH_MAIN,
  WEBPACK_FILEPATH_LIB,
  WASM_FILEPATH_SRC,
  WASM_FILEPATH_SRC_DIST,
  WASM_FILEPATH_JS_LIB
} = envVariables

// Setup a task for generating the application code that
// is optimized and ready to run.
gulp.task('build',
  task_chain([
    'clean_src_dist',
    'copy_indexHTML',
    'tslint',
    'typescript',
    'bundle_js_main',
    'bundle_js_lib',
    'wasm'
  ]))

// Setup a task for generating .wasm code from a source
// file (e.g. C, C++, Rust).
gulp.task('wasm',
  task_emscripten(WASM_FILEPATH_SRC, WASM_FILEPATH_SRC_DIST, WASM_FILEPATH_JS_LIB))

// Setup a task for linting the TypeScript source code to
// detect potential sources of errors.
gulp.task('tslint',
  task_tslint([`${PATH_SRC}/**/*.ts`]))

// Setup a task for converting the TypeScript source code to
// browser-friendly JavaScript.
gulp.task('typescript',
  task_typescript([`${PATH_SRC}/**/*.ts`], PATH_SRC_DIST))

// Setup a task to clean up the source distribution directory.
gulp.task('clean_src_dist',
  task_clean_dir(PATH_SRC_DIST))

// Setup a task to copy the index.html to the source distribution directory.
gulp.task('copy_indexHTML',
  task_copy_file(`${PATH_SRC}/index.html`, PATH_SRC_DIST))

// Setup a task to bundle the main.js file and its child dependencies
// into a single .js file.
gulp.task('bundle_js_main',
  task_bundle_js(WEBPACK_FILEPATH_MAIN))

// Setup a task to bundle the library files into a single file .js file.
gulp.task('bundle_js_lib',
  task_bundle_js(WEBPACK_FILEPATH_LIB))
