import * as path from 'path'

// Setup common path variables
const PATH_SRC = path.resolve(__dirname, '../src')
const PATH_SRC_DIST = path.resolve(__dirname, '../dist/src')
const PATH_BUILD = path.resolve(__dirname, '../build')
const PATH_BUILD_DIST = path.resolve(__dirname, '../dist/build')

// Setup Webpack variables.
const WEBPACK_FILEPATH_MAIN = path.resolve(PATH_BUILD_DIST, './webpack.main.js')
const WEBPACK_FILEPATH_LIB = path.resolve(PATH_BUILD_DIST, './webpack.lib.js')

// Setup WebAssembly variables.
const WASM_FILEPATH_SRC = path.resolve(PATH_SRC, './main.c')
const WASM_FILEPATH_SRC_DIST = path.resolve(PATH_SRC_DIST, './a.out.js')
const WASM_FILEPATH_JS_LIB = path.resolve(PATH_SRC_DIST, './lib/index.js')

// Setup Main javascript file.
const JS_FILEPATH_MAIN = path.resolve(PATH_SRC_DIST, './main.js')

export default {
  PATH_SRC,
  PATH_SRC_DIST,
  PATH_BUILD,
  PATH_BUILD_DIST,
  WEBPACK_FILEPATH_MAIN,
  WEBPACK_FILEPATH_LIB,
  WASM_FILEPATH_SRC,
  WASM_FILEPATH_SRC_DIST,
  WASM_FILEPATH_JS_LIB,
  JS_FILEPATH_MAIN
}
