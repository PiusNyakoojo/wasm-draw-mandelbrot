import envVariables from './env_variables'

// Provide the build environment variables globally for
// the rest of this file to use.
let {
  PATH_SRC_DIST,
  JS_FILEPATH_MAIN
} = envVariables

// Export Webpack configuration settings.
export default {
  entry: JS_FILEPATH_MAIN,
  output: {
    path: PATH_SRC_DIST,
    filename: 'index.js'
  }
}
