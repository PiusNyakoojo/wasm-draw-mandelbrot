## 📚 Emscripten JavaScript Library

We are using Emscripten to compile our C code to WebAssembly. If we want to call
JavaScript functions from C, we need to provide the relevant symbols to
Emscripten using the ```--js-library``` flag.

You can read more about this feature on at the [official docs](https://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html#implement-a-c-api-in-javascript).
