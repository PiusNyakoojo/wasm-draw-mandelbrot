// We need to declare these variables so the TypeScript compiler
// doesn't yell at us when we attempt to access unknown variables.
//
// mergeInto and LibraryManager are variables that are made available
// by Emscripten when we pass this file to the --js-library flag.
declare let mergeInto: any
declare let LibraryManager: any

let lib = {}

mergeInto(LibraryManager.library, lib)
