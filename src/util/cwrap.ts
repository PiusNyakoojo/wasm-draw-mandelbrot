declare let Module: any

// We will use a dictionary to quickly look up and call functions
// wrapped by Module.cwrap().
let cwrapFunctions: { [key: string]: Function } = {}

function initCWrapFunctions () {
  // The goal of this function is to intialize wrappers around WebAssembly functions
  // so our TypeScript code can call those functions when they need to.
  cwrapFunctions['functionName'] = Module.cwrap('function_name', null, [''])
}

// The following functions serves as wrappers around our cwrap functions so we can
// export them (shown below). Exporting these functions allows us to easily
// import them in other files (using import { functionName } from 'util/cwrap').

function functionName (): any {
  return cwrapFunctions['functionName']()
}

// Export our initializer and function wrappers.
export {
  initCWrapFunctions,
  functionName
}
