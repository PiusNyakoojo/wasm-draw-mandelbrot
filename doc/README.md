## 📖 Table of Contents

- [Introduction](#introduction)
- [Configuration](#configuration)
  - [TypeScript](#typescript)
  - [Unit Testing](#unit-testing)
- [Environment Variables](#environment-variables)

## Introduction

WASM Draw Mandelbrot is an example of using [WebAssembly](https://webassembly.org) to generate graphics data.

This project aims to provide an example of using WebAssembly in a fully-reusable scaffold. It ensures the various
build tools ([Gulp](https://gulpjs.com/) and [Webpack](https://webpack.js.org/)) work smoothly together with sensible defaults so you can focus on writing your app
instead of spending days wrangling with configurations. At the same time, it still offers the flexibility to tweak
the config of each tool without the need for ejecting.

The following features are provided:

- An example of using shared memory arrays. [See here]()
- An example of communicating <i>to JavaScript from WebAssembly</i> using the ```--js-library``` emscripten flag. [See here]()
- An example of communicating <i>to WebAssembly from JavaScript</i> using ```Module.cwrap()```. [See here]()

## Configuration

### TypeScript

TypeScript can be configured via `tsconfig.json`.

### Unit Testing

- #### Mocha (via `mocha-webpack`)

Check [here](./config.md) for full list of possible options.

## Environment Variables

It is possible that the naming convention of the src files is unsuitable for your needs. In that case, you may visit ```build/env_variables.ts``` to update them to your heart's desire.
